preposimmusclehmf.py
====================
Author: Ju Zhang
Last Modified: 2016-09-21

Script to embed opensim model pathpoints in host meshes

This script fits fieldwork models to opensim geometry and embed muscle and 
fitted mesh nodes in a template host mesh. Output data is used by gait2392 
muscle customisation using host mesh fitting.

This script should work for other OpenSim models with the same bodies as
Gait2392. However, you may need to update the muscle path point files in
the gait2392_musclepoints directory to match the new model. Each file in 
this directory contains the muscle path point name and generalised 
coordinates of the body named in the filename.

Ideally run from within ipython.

Author: Ju Zhang
Last Modified: 2016-09-16

Requires
--------
- GIAS2 (with VTK and preferrably with Mayavi for visualisation)