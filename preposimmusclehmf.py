"""
preposimmusclehmf.py
====================
Author: Ju Zhang
Last Modified: 2016-09-21

Script to embed opensim model pathpoints in host meshes

===============================================================================
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
===============================================================================
"""

_descStr = """Fit fieldwork models to opensim geometry and embed muscle and 
fitted mesh nodes in a template host mesh. Output data is used by gait2392 
muscle customisation using host mesh fitting.

Ideally run from within ipython.

This script should work for other OpenSim models with the same bodies as
Gait2392. However, you may need to update the muscle path point files in
the gait2392_musclepoints directory to match the new model. Each file in 
this directory contains the muscle path point name and generalised 
coordinates of the body named in the filename.

Author: Ju Zhang
Last Modified: 2016-09-16

Requires
--------
- GIAS2 (with VTK and preferrably with Mayavi for visualisation)
"""

import sys
import os
import copy
import numpy as np
import argparse
from argparse import RawTextHelpFormatter
import vtk
from gias2.mesh import vtktools
from gias2.learning import PCA
from gias2.fieldwork.field.tools import mesh_fitter
from gias2.fieldwork.field import geometric_field
from gias2.fieldwork.field import geometric_field_fitter as GFF

try:
    from gias2.visualisation import fieldvi
    vis = True
except ImportError:
    print('Warning: no visualisation possible')
    vis = False

#=============================================================================#
# 2392 geometry files
osim_geometry_path = 'gait2392_geometry/'
# files containing the name and generalised coordinates of 
# muscle path points in each body
osim_musclepoints_path = 'gait2392_musclepoints/'
# fieldwork atlas meshes
fieldwork_geometry_path = 'fw_atlas_meshes/'
# fieldwork shape models
fieldwork_sm_path = 'fw_shape_models/'
# output host meshes and xi locations of muscle path points
output_dir_path = 'gait2392_muscle_hostmeshes/'

# set up commandline arguments
model_choices = ('pelvis', 'femur-l', 'femur-r', 'tibia-l', 'tibia-r')
parser = argparse.ArgumentParser(
            description=_descStr,
            formatter_class=RawTextHelpFormatter,
            )
parser.add_argument('bodyname',
                    choices=model_choices,
                    help='name of the opensim body')
parser.add_argument('-d', '--display',
                    action='store_true',
                    help='visualise results, requires Mayavi')

#=============================================================================#
def _osim_segment_muscles(name):
    """
    Reads bone surface and muscle point data for a segment
    """

    #==================================================#
    # Precalculated for each segment: host-meshes, surface xi, muscle point xi
    data_dir = osim_musclepoints_path
    gait2392_segments = (
        'pelvis',
        'femur-l', 'femur-r',
        'tibia-l', 'tibia-r',
        )
    muscle_ptcld_file_pat = '{}.muscles.txt'
    #=================================================#

    if name not in gait2392_segments:
        raise(ValueError, "Invalid segment name.")

    # reference muscle points, Xi & labels
    osim_muscle_labels = tuple(
        np.loadtxt(
            os.path.join(data_dir, muscle_ptcld_file_pat.format(name)),
            usecols=(0,), dtype=str,
            )
        )
    osim_muscle_pts = np.loadtxt(
        os.path.join(data_dir, muscle_ptcld_file_pat.format(name)),
        usecols=(1,2,3), dtype=float,
        )

    return osim_muscle_pts, osim_muscle_labels

def _get_points(polydata):
    P = polydata.GetPoints().GetData()
    dimensions = P.GetNumberOfComponents()
    nPoints = P.GetNumberOfTuples()
    
    print 'loading %(np)i points in %(d)i dimensions'%{'np':nPoints, 'd':dimensions}
    
    if dimensions==1:
        points = np.array([P.GetTuple1(i) for i in xrange(nPoints)])
    elif dimensions==2:
        points = np.array([P.GetTuple2(i) for i in xrange(nPoints)])
    elif dimensions==3:
        points = np.array([P.GetTuple3(i) for i in xrange(nPoints)])
    elif dimensions==4:
        points = np.array([P.GetTuple4(i) for i in xrange(nPoints)])
    elif dimensions==9:
        points = np.array([P.GetTuple9(i) for i in xrange(nPoints)])
    
    return points

def read_target_geom(filenames):
    x = []
    for f in filenames:
        r = vtk.vtkXMLPolyDataReader()
        r.SetFileName(f)
        r.Update()
        polydata = r.GetOutput()
        x.append(_get_points(polydata))
        del r

    return np.vstack(x)

def write_material_points(filename, mps):
    node_fmt = '{:6d}'
    xi_fmt = '{:18.16f}'
    ndims = len(mps[0][1])
    line_fmt = node_fmt+' '+' '.join([xi_fmt]*ndims)+'\n'
    f = open(filename, 'w')
    for e, xis in mps:
        line_args = [e,]+list(xis)
        f.write(line_fmt.format(*line_args))
    f.close()

#=============================================================================#


#=============================================================================#
args = parser.parse_args()
body_name = args.bodyname

fitted_gf_nodes_file = os.path.join(output_dir_path, '{}.nodes'.format(body_name))
fitted_gf_nodes_xi_file = os.path.join(output_dir_path, '{}.nodes.xi'.format(body_name))
muscle_xi_file = os.path.join(output_dir_path, '{}.muscle.xi'.format(body_name))
hostmesh_file = os.path.join(output_dir_path, '{}.hostmesh'.format(body_name))

target_scale = 1000.0

# pelvis
if body_name=='pelvis':
    target_geom_files = [os.path.join(osim_geometry_path, 'sacrum.vtp'),
                        os.path.join(osim_geometry_path, 'pelvis.vtp'),
                        os.path.join(osim_geometry_path, 'l_pelvis.vtp'),
                        ]
    source_gf_file = os.path.join(fieldwork_geometry_path, 'pelvis_combined_cubic_mean_rigid_plsr_clm_seg_30TS_2013-09-13_okquality.geof')
    source_ens_file = os.path.join(fieldwork_geometry_path, 'pelvis_combined_cubic_flat.ens')
    source_mesh_file = os.path.join(fieldwork_geometry_path, 'pelvis_combined_cubic_flat.mesh')
    pc_file = os.path.join(fieldwork_sm_path, 'pelvis_combined_cubic_rigid_plsr_clm_seg_30TS_2013-09-13_okquality.pc')
    init_rot = [np.deg2rad(-90), np.deg2rad(0), np.deg2rad(90)]
# left femur
elif body_name=='femur-l':
    target_geom_files = [os.path.join(osim_geometry_path, 'l_femur.vtp')]
    source_gf_file = os.path.join(fieldwork_geometry_path, 'femur_right_as_left_mean_cortex_outer_2012-05-14_July2011TS_2_AB_rigid.geof')
    source_ens_file = os.path.join(fieldwork_geometry_path, 'femur_left_quartic_flat.ens')
    source_mesh_file = os.path.join(fieldwork_geometry_path, 'femur_left_quartic_flat.mesh')
    pc_file = os.path.join(fieldwork_sm_path, 'femur_right_as_left_2012-05-14_July2011TS_2_AB_rigid.pc')
    init_rot = [np.deg2rad(-90), np.deg2rad(0), np.deg2rad(90)]
# right femur
elif body_name=='femur-r':
    target_geom_files = [os.path.join(osim_geometry_path, 'femur.vtp')]
    source_gf_file = os.path.join(fieldwork_geometry_path, 'femur_right_mean_cortex_outer_2012-05-14_July2011TS_2_AB_rigid.geof')
    source_ens_file = os.path.join(fieldwork_geometry_path, 'femur_right_quartic_flat.ens')
    source_mesh_file = os.path.join(fieldwork_geometry_path, 'femur_right_quartic_flat.mesh')
    pc_file = os.path.join(fieldwork_sm_path, 'femur_right_2012-05-14_July2011TS_2_AB_rigid.pc')
    init_rot = [np.deg2rad(-90), np.deg2rad(0), np.deg2rad(90)]
# left tib fib
elif body_name=='tibia-l':
    target_geom_files = [os.path.join(osim_geometry_path, 'l_tibia.vtp'),
                        os.path.join(osim_geometry_path, 'l_fibula.vtp')
                        ]
    source_gf_file = os.path.join(fieldwork_geometry_path, 'tibia_fibula_cubic_left_mean_rigid_LLP26.geof')
    source_ens_file = os.path.join(fieldwork_geometry_path, 'tibia_fibula_left_cubic_flat.ens')
    source_mesh_file = os.path.join(fieldwork_geometry_path, 'tibia_fibula_left_cubic_flat.mesh')
    pc_file = os.path.join(fieldwork_sm_path, 'tibia_fibula_cubic_left_rigid_LLP26.pc')
    init_rot = [np.deg2rad(-90), np.deg2rad(0), np.deg2rad(90)]
# right tib fib
elif body_name=='tibia-r':
    target_geom_files = [os.path.join(osim_geometry_path, 'tibia.vtp'),
                        os.path.join(osim_geometry_path, 'fibula.vtp')
                        ]
    source_gf_file = os.path.join(fieldwork_geometry_path, 'tibia_fibula_cubic_right_mirrored_from_left_mean_rigid_LLP26.geof')
    source_ens_file = os.path.join(fieldwork_geometry_path, 'tibia_fibula_right_cubic_flat.ens')
    source_mesh_file = os.path.join(fieldwork_geometry_path, 'tibia_fibula_right_cubic_flat.mesh')
    pc_file = os.path.join(fieldwork_sm_path, 'tibia_fibula_cubic_right_mirrored_from_left_rigid_LLP26.pc')
    init_rot = [np.deg2rad(-90), np.deg2rad(0), np.deg2rad(90)]
else:
    raise ValueError('Invalid body name')

view_disc = [8,8]

#=============================================================================#
# host mesh fit MAP mean bone models to osim bone models                      #
#=============================================================================#
target_data = read_target_geom(target_geom_files)
target_data *= target_scale
source_gf = geometric_field.load_geometric_field(
                source_gf_file,
                source_ens_file,
                source_mesh_file
                )
gf = copy.deepcopy(source_gf)
pc = PCA.loadPrincipalComponents(pc_file)

fitter = mesh_fitter.MeshFitter(body_name)
fitter.setData(target_data)
fitter.setTemplateMesh(gf)

fitter.alignObjMode = 'DPEP'
fitter.alignRigid(initRotation=init_rot)
aligned_gf = copy.deepcopy(fitter.templateGF)

fitter.PCFObjMode = 'DPEP'
fitter.PCFEPD = [16,16]
fitter.PCFInitTrans = fitter.rigidTOpt[:3]
fitter.PCFInitRot = fitter.rigidTOpt[3:6]
fitter.PCFitmW = 0.001
fitter.PCFitNModes = [1,2,3,4]
fitter.PCFitXtol = 1e-9
fitter.pcFit(pc)
pcfit_gf = copy.deepcopy(fitter.templateGF)

fitter.HMFObjMode = 'DPEP'
fitter.HMFMaxIt = 10
fitter.HMFMaxItPerIt = 2
fitter.HMFHostElemType = 'quad444'
fitter.HMFHostElemDisc = [1,1,1]
fitter.HMFHostSobD = [4,4,4]
fitter.HMFHostSobW = 1e-5
fitter.HMFSlaveEPD = [10,10]
fitter.HMFSlaveSobD = [5,5]
fitter.HMFSlaveSobW = np.array([1,1,1,1,2])*1e-8
fitter.HMFSlaveNormD = 5
fitter.HMFSlaveNormW = 100.0
fitter.HMFXtol = 1e-6
fitter.HMFTreeArgs = {'distance_upper_bound':50.0}
fitter.HMF()
hmf_gf = copy.deepcopy(fitter.templateGF)

# final_gf = aligned_gf
final_gf = hmf_gf

#=============================================================================#
# Create host mesh around fitted points and muscle points                     #
#=============================================================================#
osim_hm_pad = 5.0
osim_hm_type = 'quad444'
osim_hm_elems = [1,1,1]

muscle_pts, muscle_labels = _osim_segment_muscles(body_name)
muscle_pts *= target_scale
bone_pts = final_gf.get_all_point_positions()
osim_bone_muscle = np.vstack([muscle_pts, bone_pts])

osim_host_mesh = GFF.makeHostMeshMulti(
        osim_bone_muscle.T,
        osim_hm_pad,
        osim_hm_type,
        osim_hm_elems,
    )

# calculate the emdedding (xi) coordinates of muscle points
osim_muscle_xi = osim_host_mesh.find_closest_material_points(
    muscle_pts,
    initGD=[50,50,50],
    verbose=True,
    )[0]
# osim_muscle_xi = [xi[1] for xi in osim_muscle_xi]

# calculate the emdedding (xi) coordinates of fitted bone mesh nodes
osim_bone_xi = osim_host_mesh.find_closest_material_points(
    bone_pts,
    initGD=[50,50,50],
    verbose=True,
    )[0]
# osim_bone_xi = [xi[1] for xi in osim_bone_xi]

# TEST: eval xis
eval_bone_pts = geometric_field.makeGeometricFieldEvaluatorSparse(
    osim_host_mesh, [1,1],
    matPoints=osim_bone_xi,
    )
bone_pts_eval = eval_bone_pts(osim_host_mesh.field_parameters).T
eval_muscle_pts = geometric_field.makeGeometricFieldEvaluatorSparse(
    osim_host_mesh, [1,1],
    matPoints=osim_muscle_xi,
    )
muscle_pts_eval = eval_muscle_pts(osim_host_mesh.field_parameters).T

#=============================================================================#
# save fitted nodes, fitted nodes xi, muscle xi                               #
#=============================================================================#
# SAVE IN MM

# save fitted nodes
np.savetxt(fitted_gf_nodes_file, bone_pts)

# save muscle xi
write_material_points(fitted_gf_nodes_xi_file, osim_bone_xi)

# save bone xi
write_material_points(muscle_xi_file, osim_muscle_xi)

# save host mesh
osim_host_mesh.save_geometric_field(
    hostmesh_file, hostmesh_file, hostmesh_file
    )

#=============================================================================#
# Visualise                                                                   #
#=============================================================================#
if vis and args.display:
    v = fieldvi.Fieldvi()
    v.displayGFNodes = False
    v.GFD = view_disc
    v.addData('target', target_data, renderArgs={'scale_factor':3.0})
    gf_eval = geometric_field.makeGeometricFieldEvaluatorSparse(gf, view_disc)
    v.addGeometricField('final gf', final_gf, gf_eval)

    v.addData('fitted bone nodes', bone_pts, renderArgs={'mode':'sphere','color':(0,1,0), 'scale_factor':3.0})
    v.addData('muscle points', muscle_pts, renderArgs={'mode':'sphere','color':(1,0,0), 'scale_factor':5.0})
    v.addData('fitted bone nodes eval', bone_pts, renderArgs={'mode':'sphere','color':(0.5,1,0.5), 'scale_factor':3.0})
    v.addData('muscle points eval', muscle_pts, renderArgs={'mode':'sphere','color':(1,0.5,0.5), 'scale_factor':5.0})

    v.configure_traits()
    v.scene.background = (0,0,0)
